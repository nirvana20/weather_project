# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Day',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city_id', models.IntegerField()),
                ('month', models.IntegerField()),
                ('day', models.IntegerField()),
                ('year', models.IntegerField()),
                ('mean_temp', models.FloatField(default=b'', max_length=20)),
                ('max_temp', models.FloatField(default=b'', max_length=20)),
                ('min_temp', models.FloatField(default=b'', max_length=20)),
                ('mean_humidity', models.FloatField(default=b'', max_length=20)),
                ('max_humidity', models.FloatField(default=b'', max_length=20)),
                ('min_humidity', models.FloatField(default=b'', max_length=20)),
            ],
        ),
        migrations.RemoveField(
            model_name='city',
            name='day',
        ),
        migrations.RemoveField(
            model_name='city',
            name='max_humidity',
        ),
        migrations.RemoveField(
            model_name='city',
            name='max_temp',
        ),
        migrations.RemoveField(
            model_name='city',
            name='mean_humidity',
        ),
        migrations.RemoveField(
            model_name='city',
            name='mean_temp',
        ),
        migrations.RemoveField(
            model_name='city',
            name='min_humidity',
        ),
        migrations.RemoveField(
            model_name='city',
            name='min_temp',
        ),
        migrations.RemoveField(
            model_name='city',
            name='month',
        ),
        migrations.RemoveField(
            model_name='city',
            name='year',
        ),
        migrations.AddField(
            model_name='details',
            name='day_id',
            field=models.IntegerField(default=-1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='city',
            name='city_name',
            field=models.CharField(unique=True, max_length=50),
        ),
    ]
