# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_name', models.CharField(default=b'', max_length=50, blank=True)),
                ('city_name', models.CharField(max_length=50)),
                ('month', models.IntegerField()),
                ('day', models.IntegerField()),
                ('year', models.IntegerField()),
                ('mean_temp', models.FloatField(default=b'', max_length=20)),
                ('max_temp', models.FloatField(default=b'', max_length=20)),
                ('min_temp', models.FloatField(default=b'', max_length=20)),
                ('mean_humidity', models.FloatField(default=b'', max_length=20)),
                ('max_humidity', models.FloatField(default=b'', max_length=20)),
                ('min_humidity', models.FloatField(default=b'', max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Details',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city_id', models.IntegerField()),
                ('hour', models.IntegerField()),
                ('minute', models.IntegerField()),
                ('temp', models.FloatField()),
                ('humidity', models.FloatField()),
            ],
        ),
    ]
