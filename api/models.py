from django.db import models

# Create your models here.

class City(models.Model):
    country_name = models.CharField(max_length=50,default='',blank=True)
    city_name = models.CharField(max_length=50,unique=True)


class DayData(models.Model):
    city_id = models.IntegerField()
    month = models.IntegerField()
    day = models.IntegerField()
    year = models.IntegerField()
    mean_temp = models.FloatField(max_length=20,default='')
    max_temp = models.FloatField(max_length=20,default='')
    min_temp = models.FloatField(max_length=20,default='')
    mean_humidity = models.FloatField(max_length=20,default='')
    max_humidity = models.FloatField(max_length=20,default='')
    min_humidity = models.FloatField(max_length=20,default='')

class Details(models.Model):
    city_id = models.IntegerField()
    day_id  = models.IntegerField()
    hour = models.IntegerField()
    minute = models.IntegerField()
    temp = models.FloatField()
    humidity = models.FloatField()


