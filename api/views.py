from django.http import HttpResponse
from rest_framework import status
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
import urllib2
import json
from models import City,Details,DayData
from datetime import datetime, timedelta,date
# Create your views here.


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


class Home(APIView):

    def get(self, request):
        # City.objects.all().delete()
        # Details.objects.all().delete()
        # DayData.objects.all().delete()
        l = []
        obj = City.objects.all()
        for data in obj:
            l.append(data.city_name)
        l = list(set(l))
        return render(request, 'home.html', {'list':l})


class ChartData(APIView):

    def get(self, request):
        msg = request.GET['msg']
        if msg == '0':
            country_name = request.GET['country_name']
        city_name = request.GET['city_name']
        from_month = request.GET['from_month']
        from_year = request.GET['from_year']
        from_day = request.GET['from_day']
        to_month = request.GET['to_month']
        to_year = request.GET['to_year']
        to_day = request.GET['to_day']
        dataselect = request.GET['dataselect']
        d1 = date(int(from_year),int(from_month),int(from_day))
        d2 = date(int(to_year),int(to_month),int(to_day)+1)
        delta = d2-d1
        l = []
        flag = 0
        if int(to_day)-int(from_day)==0:
            flag = 1

        for single_date in daterange(d1, d2):
            if single_date.month<10 and single_date.day<10:
                ss=str(single_date.year)+'0'+str(single_date.month)+'0'+str(single_date.day)
            elif single_date.month<10:
                ss=str(single_date.year)+'0'+str(single_date.month)+str(single_date.day)
            elif single_date.day<10:
                ss=str(single_date.year)+str(single_date.month)+'0'+str(single_date.day)
            else:
                ss=str(single_date.year)+str(single_date.month)+str(single_date.day)
            ss = str(ss)
            if msg == '0':
                city_cc = City.objects.filter(country_name=country_name,city_name=city_name)
            else:
                city_cc = City.objects.filter(city_name=city_name)
            if city_cc:
                if DayData.objects.filter(city_id=city_cc[0].id, month=single_date.month,year=single_date.year,day=single_date.day):
                    day_data = DayData.objects.filter(city_id=city_cc[0].id, month=single_date.month,year=single_date.year,day=single_date.day)

                    if flag == 1 and Details.objects.filter(city_id=city_cc[0].id,day_id=day_data[0].id):
                        dd = Details.objects.filter(city_id=city_cc[0].id,day_id=day_data[0].id)
                        lst = []
                        for data in dd:
                            time = data.hour+float(data.minute)/100.00
                            time = float(time)
                            temp = data.temp
                            humidity = data.humidity
                            if dataselect == 'Temperature':
                                lst.append({'temp': temp, 'hum': humidity, 'time': time})
                            else:
                                lst.append({'temp': humidity, 'hum': humidity, 'time': time})
                        return Response(lst)
                    if dataselect == 'Temperature':
                        l.append({'temp': day_data[0].mean_temp, 'hum': day_data[0].max_humidity, 'time': single_date.day})
                    else:
                        l.append({'temp': day_data[0].max_humidity, 'hum': day_data[0].max_humidity, 'time': single_date.day})
                else:
                    if msg == '0':
                        url = 'http://api.wunderground.com/api/df6321adac3da44d/history_'+ss+'/q/'+country_name+'/' +\
                              city_name+'.json'
                    else:
                        url = 'http://api.wunderground.com/api/df6321adac3da44d/history_'+ss+'/q/CA/'+city_name+'.json'
                    f= urllib2.urlopen(url)
                    json_string = f.read()
                    parsed_json = json.loads(json_string)
                    # print parsed_json
                    mean_temp = 0
                    max_temp = 0
                    min_temp = 0
                    mean_humidity = 0
                    max_humidity = 0
                    min_humidity = 0
                    for data in parsed_json['history']['dailysummary']:
                        mean_temp = data['meantempm']
                        min_temp = data['mintempm']
                        max_temp = data['maxtempm']
                        mean_humidity = data['humidity']
                        max_humidity = data['maxhumidity']
                        min_humidity = data['minhumidity']
                    # print DayData.objects.filter(city_id=city_cc[0].id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                    # if not DayData.objects.filter(city_id=city_id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity):
                    #     print "sdsd"
                    day_object = DayData(city_id=city_cc[0].id, month=single_date.month,year=single_date.year,day=single_date.day,
                                         max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,
                                         mean_humidity=mean_humidity,min_humidity=min_humidity)
                    # day_object.save()
                    # day_id = day_object.id
                    #     day_object = DayData.objects.filter(city_id=city_id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                    #     day_id = day_object[0].id
                    p = []
                    for data in parsed_json['history']['observations']:
                        # if not Details.objects.filter(city_id=city.id,day_id=day_id, hour = data['date']['hour'], minute= data['date']['min'], temp=data['tempi'],humidity=data['hum'] ):
                        # details = Details(city_id=city_cc[0].id,day_id=day_id, hour = data['date']['hour'], minute= data['date']['min'], temp=data['tempi'],humidity=data['hum'] )
                        # details.save()
                        time = float(data['date']['hour'])+float(data['date']['min'])/100.00
                        time = float(time)
                        temp = data['tempm']
                        humidity = data['hum']

                        if dataselect == "Temperature" and flag == 1:
                            p.append({'temp': temp, 'hum': humidity, 'time': time})
                        elif flag == 1:
                            p.append({'temp': humidity, 'hum': humidity, 'time': time})
                    if flag == 1:
                        return Response(p)
                    if dataselect == 'Temperature':
                        l.append({'temp': mean_temp, 'hum': max_humidity, 'time': single_date.day})
                    else:
                        l.append({'temp': max_humidity, 'hum': max_humidity, 'time': single_date.day})
                    f.close()
            else:
                if msg == '0':
                    url = 'http://api.wunderground.com/api/df6321adac3da44d/history_'+ss+'/q/'+country_name+'/' +city_name+'.json'
                else:
                    url = 'http://api.wunderground.com/api/df6321adac3da44d/history_'+ss+'/q/CA/'+city_name+'.json'
                f= urllib2.urlopen(url)
                json_string = f.read()
                parsed_json = json.loads(json_string)
                # print parsed_json
                mean_temp = 0
                max_temp = 0
                min_temp = 0
                mean_humidity = 0
                max_humidity = 0
                min_humidity = 0
                for data in parsed_json['history']['dailysummary']:
                    mean_temp = data['meantempm']
                    min_temp = data['mintempm']
                    max_temp = data['maxtempm']
                    mean_humidity = data['humidity']
                    max_humidity = data['maxhumidity']
                    min_humidity = data['minhumidity']
                if msg == '0':
                        # city = City(country_name=country_name,city_name=city_name, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                    if not City.objects.filter(country_name=country_name,city_name=city_name):
                        city = City(country_name=country_name,city_name=city_name)
                        city.save()
                        city_id = city.id
                    else:
                        city = City.objects.get(country_name=country_name,city_name=city_name)
                        city_id = city.id
                        # print city.id
                elif not  City.objects.filter(city_name=city_name):
                    city = City(city_name=city_name)
                    city.save()
                    city_id = city.id
                else:
                    city = City.objects.get(city_name=city_name)
                    city_id = city.id
                if not DayData.objects.filter(city_id=city_id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity):
                    day_object = DayData(city_id=city_id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                    day_object.save()
                    day_id = day_object.id
                else:
                    day_object = DayData.objects.filter(city_id=city_id, month=single_date.month,year=single_date.year,day=single_date.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                    day_id = day_object[0].id
                p = []
                for data in parsed_json['history']['observations']:
                    if not Details.objects.filter(city_id=city.id,day_id=day_id, hour = data['date']['hour'], minute= data['date']['min'], temp=data['tempm'],humidity=data['hum'] ):
                        details = Details(city_id=city.id,day_id=day_id, hour = data['date']['hour'], minute= data['date']['min'], temp=data['tempm'],humidity=data['hum'] )
                        details.save()
                    time = float(data['date']['hour'])+float(data['date']['min'])/100.00
                    time = float(time)
                    temp = data['tempm']
                    humidity = data['hum']
                    if dataselect == "Temperature" and flag == 1:
                        p.append({'temp': temp, 'hum': humidity, 'time': time})
                    elif flag == 1:
                        p.append({'temp': humidity, 'hum': humidity, 'time': time})
                if flag == 1:
                    return Response(p)
                if dataselect == 'Temperature':
                    l.append({'temp': mean_temp, 'hum': max_humidity, 'time': single_date.day})
                else:
                    l.append({'temp': mean_humidity, 'hum': max_humidity, 'time': single_date.day})
        return Response(l)


class AddnewData(APIView):

    def post(self, request):
        city_name = request.POST['city_name']
        cc = City.objects.filter(city_name=city_name)
        if cc:
            msg = {"status": "Already Exists"}
            return HttpResponse(json.dumps(msg), status=status.HTTP_200_OK)
        dd = datetime.now()
        ss = str(dd.year)+str(dd.month)+str(dd.day)
        url = 'http://api.wunderground.com/api/df6321adac3da44d/history_'+ss+'/q/CA/'+city_name+'.json'
        f= urllib2.urlopen(url)
        json_string = f.read()
        parsed_json = json.loads(json_string)
        mean_temp = 0
        max_temp = 0
        min_temp = 0
        mean_humidity = 0
        max_humidity = 0
        min_humidity = 0
        l = []
        for data in parsed_json['history']['dailysummary']:
            mean_temp = data['meantempm']
            min_temp = data['mintempm']
            max_temp = data['maxtempm']
            mean_humidity = data['maxhumidity']
            max_humidity = data['maxhumidity']
            min_humidity = data['minhumidity']
            if City.objects.filter(city_name=city_name):
                cc = City.objects.filter(city_name=city_name)
                city_id = cc[0].id
            else:
                city = City(city_name=city_name)
                city.save()
                city_id  = city.id
            if not DayData.objects.filter(city_id=city_id,month=dd.month,year=dd.year,day=dd.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity):
                day_data = DayData(city_id=city_id,month=dd.month,year=dd.year,day=dd.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                day_data.save()
                day_id = day_data.id
            else:
                day_data = DayData.objects.filter(city_id=city_id,month=dd.month,year=dd.year,day=dd.day,max_temp=max_temp,min_temp=min_temp,mean_temp=mean_temp,max_humidity=max_humidity,mean_humidity=mean_humidity,min_humidity=min_humidity)
                day_id = day_data[0].id
            for data in parsed_json['history']['observations']:
                details = Details(city_id=city_id,day_id=day_id, hour = data['date']['hour'], minute= data['date']['min'], temp=data['tempm'],humidity=data['hum'] )
                details.save()
        msg = {"status": "Added Successfully"}
        return HttpResponse(json.dumps(msg), status=status.HTTP_200_OK)

