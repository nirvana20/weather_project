from django.conf.urls import include, url
from django.contrib import admin
from . import views
urlpatterns = [
    # Examples:
    # url(r'^$', 'weather.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^home/$', (views.Home.as_view())),
    url(r'^chartdata/$', (views.ChartData.as_view())),
    url(r'^addnewcity/$',(views.AddnewData.as_view())),
]
